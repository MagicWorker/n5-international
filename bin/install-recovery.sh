#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/msm_sdcc.1/by-name/recovery:12566528:cd35080e853d3523ccaca7c9f0e6c75bfcd54f45; then
  applypatch EMMC:/dev/block/platform/msm_sdcc.1/by-name/boot:10805248:e9dcdecbd30eb174da3e66ef652c810eb7b4aa67 EMMC:/dev/block/platform/msm_sdcc.1/by-name/recovery cd35080e853d3523ccaca7c9f0e6c75bfcd54f45 12566528 e9dcdecbd30eb174da3e66ef652c810eb7b4aa67:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
